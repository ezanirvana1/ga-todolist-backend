const EvenEmitter = require('events')
const Auth = new EvenEmitter

process.log.users = {}

Auth.on('unauthorized', ({ _id, email, source }) => {
    if (!process.log.users[_id]) {
        process.log.users[_id] = {
            email,
            source,
            count: 1
        }
        return console.log('Initiate:', process.log.users[_id])
    }

    process.log.users[_id].count++

    if(process.log.users[_id].count > 4){
        Auth.emit('parahaNih', process.log.users[_id], process.log.email)
    }

    console.log('Final:', process.log.users[_id])
})


Auth.on('parahaNih', email => {
    console.log(`sending email to ${email}`)
})

module.exports = Auth