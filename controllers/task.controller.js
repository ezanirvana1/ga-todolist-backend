const jwt = require('jsonwebtoken');
const Task = require('../models/task.model');
const { success, fail } = require("../helpers/res");

// create task // done // ok
exports.create = (req, res) => {
    let payload = jwt.verify(req.headers.authorization, process.env.SECRET_KEY)
    let task = new Task({
        name: req.body.name,
        description: req.body.description,
        owner: payload._id,
        dueDate: req.body.dueDate
    })
    Task.create(task)
        .then(data => success(res, data, 201))
        .catch(err => fail(res, err, 422))
}

// find user task // done // ok
exports.findUserTask = (req, res) => {
    let page = parseInt(req.query.page, 10)
    let limit = parseInt(req.query.limit, 10)
    let payload = jwt.verify(req.headers.authorization, process.env.SECRET_KEY)
    Task.paginate({ owner: payload._id }, {page, limit})
        .then(data => {
            if (!data) return fail(res, `Task not found!`, 422)
            success(res, data, 200)
        })
        .catch(err => fail(res, err, 422))
}

// change completion value // done // ok
exports.isComplete = (req, res) => {
    let payload = jwt.verify(req.headers.authorization, process.env.SECRET_KEY)
    Task.findOne({ owner: payload._id, _id: req.params.taskId })
        .then(data => {
            if (!data) return fail(res, `Task not found!`, 422)
            Task.findByIdAndUpdate({ _id: data._id }, { completed: !data.completed })
                .then(() => success(res, `completed ${!data.completed}`, 200))
                .catch(err => fail(res, err, 422))
        })
        .catch(err => fail(res, err, 422))
}

// find by complete // ok
exports.findByComplete = (req, res) => {
    let page = parseInt(req.query.page, 10)
    let limit = parseInt(req.query.limit, 10)
    let payload = jwt.verify(req.headers.authorization, process.env.SECRET_KEY)
    Task.paginate({ owner: payload._id, completed: true }, { page, limit })
        .then(data => {
            if (!data) return fail(res, `Task not found!`, 422)
            success(res, data, 200)
        })
        .catch(err => fail(res, err, 422))
}

// change importance value // done // ok
exports.isImportance = (req, res) => {
    let payload = jwt.verify(req.headers.authorization, process.env.SECRET_KEY)
    Task.findOne({ owner: payload._id, _id: req.params.taskId })
        .then(data => {
            Task.findByIdAndUpdate({ _id: data._id }, { importance: !data.importance })
                .then(() => success(res, `importance ${!data.importance}`, 200))
                .catch(err => fail(res, err, 422))
        })
        .catch(err => fail(res, err, 422))
}

// find by importance // done // ok
exports.findByImportance = (req, res) => {
    let page = parseInt(req.query.page, 10)
    let limit = parseInt(req.query.limit, 10)
    let payload = jwt.verify(req.headers.authorization, process.env.SECRET_KEY)
    Task.paginate({ owner: payload._id, importance: true }, { page, limit })
        .then(data => {
            if (!data) return fail(res, `Task not found!`, 422)
            success(res, data, 200)
        })
        .catch(err => fail(res, err, 422))
}

// delete user task // done //  ok
exports.delete = (req, res) => {
    Task.findOneAndDelete({ _id: req.params.taskId })
        .then(data => {
            if (!data) return fail(res, `Task not found!`, 422)
            success(res, `${data.name} has been deleted!`, 200)
        })
        .catch(err => fail(res, err, 422))
}

// update task // done // ok
exports.update = (req, res) => {
    let payload = jwt.verify(req.headers.authorization, process.env.SECRET_KEY)
    Task.findOneAndUpdate(({ owner: payload._id, _id: req.params.taskId }), req.body)
        .then(data => {
            if (!data) return fail(res, `Task not found!`, 422)
            success(res, 'berhasil diupdate', 200)
        })
        .catch(err => fail(res, err, 422))
}