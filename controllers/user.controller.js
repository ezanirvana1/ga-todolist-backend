const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const User = require("../models/user.model");
const { success, fail } = require("../helpers/res");

const mailer = require("../middlewares/mailer");
const Imagekit = require("imagekit");

const imagekitInstance = new Imagekit({
  publicKey: process.env.IMAGEKIT_PUBLIC_KEY,
  privateKey: process.env.IMAGEKIT_PRIVATE_KEY,
  urlEndpoint: `https://ik.imagekit.io/${process.env.IMAGEKIT_ID}`
});

exports.uploadPhoto = (req, res) => {
  imagekitInstance
    .upload({
      file: req.file.buffer.toString("base64"),
      fileName: `IMG-${Date()}`
    })
    .then(async data => {
      User.findOneAndUpdate({ _id: req.user._id }, { image: data.url }, (error, document, result) => {
        let newResponse = {
          ...document._doc,
          image: data.url
        }
        delete newResponse.password
        success(res, newResponse, 200)
      })
    })
    .catch(err => {
      fail(res, err, 422);
    });
};

// edit profile // done
exports.editProfile = (req, res) => {
  let user = jwt.verify(req.headers.authorization, process.env.SECRET_KEY);

  let newData = {
    name: req.body.name,
    email: req.body.email
  };

  User.findOne({ _id: user._id })
    .then(data => {
      if (!newData.name) newData.name = data.name
      if (!newData.email) newData.email = data.email
      let isPasswordValid = bcrypt.compareSync(req.body.password, data.password)
      if (!isPasswordValid) return fail(res, 'Wrong password', 422)
      User.findOneAndUpdate({ _id: user._id }, newData)
        .then(() => success(res, "sukses update", 200))
        .catch(err => fail(res, err, 422));
    })
    .catch(err => fail(res, err, 422));
};

// register new user // done
exports.registerUser = (req, res) => {
  if (req.body.password != req.body.password_confirmation)
    return fail(res, "Password doesn't match", 422);

  let user = new User({
    name: req.body.name,
    email: req.body.email,
    password: bcrypt.hashSync(req.body.password, 10)
  });

  User.create(user)
    .then(data => {
      let token = jwt.sign({ _id: data._id }, process.env.SECRET_KEY);

      return mailer.send({
        from: "no-reply@ga-todolist.com",
        to: data.email,
        subject: "User Activation",
        html: `
        <p> Hei, ${data.name}. Please click on the link below to verify your email and continue the registration process.</p>
        <a href="${process.env.BASE_URL}/api/user/activation/${token}">Click here</a>`
      });
    })
    .then(() => {
      if ((env = "TEST")) {
        return res.status(201).json({
          message:
            "A verification link has been sent to your email account. Please click on the link that has just been sent to your email account to verify your email and continue the registration process.",
          verifyToken: token
        });
      }
      success(
        res,
        `A verification link has been sent to your email account. Please click on the link that has just been sent to your email account to verify your email and continue the registration process.`,
        201
      );
    })
    .catch(err => fail(res, err, 422));
};

// activate user // done
exports.userActivation = (req, res) => {
  let user = jwt.verify(req.params.token, process.env.SECRET_KEY);

  User.findOneAndUpdate({ _id: user._id }, { isConfirmed: true })
    .then(() => {
      success(res, "Your email is verified now.", 201);
    })
    .catch(err => {
      fail(res, err, 422);
    });
};

// login user // done
exports.loginUser = (req, res) => {
  User.findOne({ email: req.body.email })
    .then(data => {
      if (!data) return fail(res, "email not registered", 403);

      let isPasswordValid = bcrypt.compareSync(
        req.body.password,
        data.password
      );
      if (!isPasswordValid) return fail(res, "wrong password", 403);

      if (!data.isConfirmed)
        return fail(res, "Email isn't verified, please check your email!", 403);

      // generate token
      let token = jwt.sign({ _id: data._id }, process.env.SECRET_KEY);

      // success respon
      success(
        res,
        {
          isConfirmed: data.isConfirmed,
          name: data.name,
          token
        },
        200
      );
    })
    .catch(err => {
      fail(res, err, 422);
    });
};

// request link to change password // done
exports.forgotPassword = (req, res) => {
  User.findOne({ email: req.body.email })
    .then(data => {
      if (!data) return fail(res, "email not registered", 403);

      let token = jwt.sign({ _id: data._id }, process.env.SECRET_KEY);

      return mailer.send({
        from: "no-reply@ga-todolist.com",
        to: data.email,
        subject: "Reset password",
        html: `
        <p> Hai, ${data.name}. segera ganti password anda dengan menekan tombol dibawah.</p>
        <a href="${process.env.BASE_URL}/api/user/resetPassword/${token}">Click here</a>`
      });
    })
    .then(() => {
      success(
        res,
        "PLEASE CHECK YOUR EMAIL AND CLICK THE CONFIRMATION LINK",
        200
      );
    })
    .catch(err => {
      fail(res, err, 422);
    });
};

// reset password // done
exports.resetPassword = (req, res) => {
  let user = jwt.verify(req.params.token, process.env.SECRET_KEY);

  if (req.body.password != req.body.password_confirmation)
    return fail(res, "Password doesn't match!", 403);

  let password = bcrypt.hashSync(req.body.password);

  User.findOneAndUpdate({ _id: user._id }, { password: password })
    .then(() => {
      success(res, "successfully changed password", 201);
    })
    .catch(() => {
      fail(res, err, 422);
    });
};

// user profile // done
exports.userProfile = (req, res) => {
  let user = jwt.verify(req.headers.authorization, process.env.SECRET_KEY);

  User.findOne({ _id: user._id })
    .then(data => success(res, data, 200))
    .catch(err => fail(res, err, 422));
};
