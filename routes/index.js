const express = require('express');
const router = express.Router();
const user = require ('./user.router');
const task = require('./task.router')

router.use('/user', user)
router.use('/task', task)

module.exports = router;

