const express = require('express');
const router = express.Router();
const validate = require('../middlewares/auth.js')

const task = require('../controllers/task.controller')

router.post('/create', validate, task.create)
router.put('/complete/:taskId', validate, task.isComplete)
router.put('/importance/:taskId', validate, task.isImportance)
router.get('/my-task', validate, task.findUserTask)
router.get('/complete', validate, task.findByComplete)
router.get('/importance', validate, task.findByImportance)
router.delete('/delete/:taskId', validate, task.delete)
router.put('/update/:taskId', validate, task.update)

module.exports = router;