const express = require('express');
const router = express.Router();
const upload = require ('../middlewares/uploader');
const validate = require('../middlewares/auth.js');
const user = require('../controllers/user.controller');

router.post('/register', user.registerUser);
router.post('/login', user.loginUser);
router.post('/upload', validate, upload, user.uploadPhoto);
router.put('/editProfile', validate, user.editProfile);

router.post('/forgotPassword', user.forgotPassword)
router.put('/resetPassword/:token', user.resetPassword)

router.get('/activation/:token', user.userActivation) 
router.get('/profile', validate, user.userProfile)

module.exports = router;

