const chai = require("chai");
const chaiHttp = require("chai-http");

const app = require("../server");
const User = require("../models/user.model");
const expect = chai.expect;

chai.use(chaiHttp);
var tokenverify;
var token;
describe("USER", function() {
  before(function() {
    User.deleteMany({}, function(err) {});
  });

  it("should register a new user", function() {
    chai
      .request(app)
      .post("/api/user/register")
      .set("Content-Type", "application/json")
      .send({
        name: "Reza Nirvana Pratama",
        email: "mrezanirvana@gmail.com",
        password: "testing01",
        password_confirmation: "testing01"
      })
      .end(function(err, res) {
        tokenverify = res.body.verifyToken
        expect(res.status).eql(201);
      });
  });

  it("should fail register a new user with does not match password", function() {
    chai
      .request(app)
      .post(`/api/user/verification/${tokenverify}`)
      .send({
        name: "Reza Nirvana Pratama",
        email: "mrezanirvana@gmail.com",
        password: "testing01",
        password_confirmation: "notmatchtesting01"
      })
      .end(function(err, res) {
        expect(res.status).eql(404);
      });
  });

  it("should fail register user with existed email", function() {
    chai
      .request(app)
      .post("/api/user/register")
      .send({
        name: "Reza Nirvana Pratama",
        email: "mrezanirvana@gmail.com",
        password: "12345",
        password_confirmation: "12345"
      })
      .end(function(err, res) {
        expect(res.status).eql(422);
      });
  });

  it("should be success login", async function() {
    chai
      .request(app)
      .post("/api/user/login")
      .set("Content-Type", "application/json")
      .send({
        email: "mrezanirvana@gmail.com",
        password: "12345"
      })
      .end(function(err, res) {
        token = res.header.authorization
        expect(res.status).eql(200);
      });
  });
});
