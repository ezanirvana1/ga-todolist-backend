const mongoose = require('mongoose');
const Schema = mongoose.Schema

const userSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    image: {
        type: 'String'
    },
    isConfirmed: {
        type: Boolean,
        default: false
    }
}, {
    timestamps: true
});

userSchema.methods.toJSON = function () {
    const user = this;

    const userObject = user.toObject();
    delete userObject.password;

    return userObject;
};


module.exports = mongoose.model('User', userSchema)