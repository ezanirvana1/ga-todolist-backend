const mongoose = require('mongoose')
const Schema = mongoose.Schema
const mongoosePaginate = require('mongoose-paginate-v2');

const taskSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    description: {
        type: String
    },
    importance: {
        type: Boolean,
        default: false
    },
    completed: {
        type: Boolean,
        default: false
    },
    owner: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    dueDate: {
        type: Date
    }
})

taskSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('Task', taskSchema)